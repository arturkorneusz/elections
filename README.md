﻿# Wybory - zadanie rekrutacyjne #


Proszę rozwiązać poniższe zadanie, projektując na jego potrzeby odpowiednią klasę.

W niektórych niemieckich okregach wyborczych miejsca do parlamentu są przydzielane na podanej zasadzie w dwóch krokach:

1. Liczba głosów na daną partię jest mnożona przez wszystkie wolne miejsca w parlamencie i następnie dzielona przez sumę wszystkich oddanych głosów

2. W drugim kroku rezultaty z pierwszego kroku są rozbijane na część całkowitą i resztę. Cześć całkowita wstępnie rerezentuje ilość miejsc dla każdej z partii. Wolne miejsca są przydzielane w kolejności od największej części po przecinku.

Przykładowe dane wejściowe:

Wszystkih miejsc: 15

Oddane głosy: A => 15000, B => 5400, C => 5500, D => 5550


Przykład obliczeń:

Krok pierwszy:

A = > 15000 * 15 /31450 = 7.1542

B = > 5400  * 15 /31450 = 2.5755

C = > 5500  * 15 /31450 = 2.6232

D = > 5550  * 15 /31450 = 2.6470


Krok drugi:

A -> 7 -> 0.1542

B -> 2 -> 0.5755

C -> 2 -> 0.6232

D -> 2 -> 0.6470


Dystrybuujemy miejsca po części całkowitej: A -> 7, B -> 2, C -> 2, D -> 2

Z tego wynika że 13 z 15 miejsc zostało obsadzonych, co oznacza że zostały dwa miejsca do obsadzenia.

Piersze miejsce trafia do partii D ponieważ ma największa resztę (0.6470) kolejne i w tym przypadku ostatnie do partii C ponieważ ma drugą co do wielkości resztę (0.6232). 


Rezultat końcowy: 
A -> 7, B -> 2, C -> 2+1, D -> 2+1


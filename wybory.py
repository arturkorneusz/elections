import math

class Wybory:
    def __init__(self, miejsca, glosy):
        self.miejsca = miejsca
        self.glosy = glosy
    
    def __calkowita(self):
        b = {}
        for a in self.glosy:
            b[a] = math.floor(self.glosy[a] * 15 / sum(self.glosy.values()))
        return b

    def __ulamkowa(self):
        b = {}
        for a in self.glosy:
            b[a] = round((self.glosy[a] * 15 / sum(self.glosy.values())) - (math.floor(self.glosy[a] * 15 / sum(self.glosy.values()))),2)
        return b       

    def __dodatkowe(self):
        a = self.__calkowita()
        b = self.__ulamkowa()
        c = {}
        pozostale = self.miejsca - sum(list(a.values()))
        for n in range(pozostale):
            c[max(b)] = 1
            del(b[max(b)])
        for n in b:
            c[n] = 0  
        return dict(sorted(c.items(), key=lambda x: x[0]))

    def wyniki(self):
        a = self.__calkowita()
        b = self.__dodatkowe()
        c = {}  
        for n in a:
            c[n] = a[n] + b[n]  
        return c    

def main():
    miejsca = 15
    glosy = {'A':15000, 'B':5400, 'C':5500, 'D':5550}
    a = Wybory(miejsca, glosy)
    print(a.wyniki())

def test_wybory():
    assert Wybory(15, {'A':15000, 'B':5400, 'C':5500, 'D':5550}).wyniki() == {'A': 7, 'B': 2, 'C': 3, 'D': 3}

if __name__ == "__main__":
    main()

